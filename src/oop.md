# Object-Oriented Programming
Fox, like many languages these days, is multi-paradigm, and as such it also features constructs to do OOP; with
first-class support for classes, interfaces, inheritance, polymorphism and sealed/abstract classes.

Classes
-
Classes are the base of OO, they represent templates on which objects are constructed upon; so let's see a simple class in Fox:
```fox
pub class Pet {
    pub Pet( this.name: String, this.age: u16 )
}
```

Inheritance
-
```fox
pub class Dog : Pet {
    pub Dog( name: String, ... ) : Pet( name, ... )
    pub fun bark( &this ) { ... }
    pub override fun play( &mut this, with: Entity ) { ... }
    pub override fun eat( &mut this, food: Food ) { ... }
    pub override fun drink( &mut this, liquid: Liquid ) { ... }
    pub override fun sleep( &mut this, amount: Hour ) { ... }
}
```
Now that's a wall of text! Although the meaning should be quite intuitive for most what each line does and what's happening;
but we'll go through it anyway. :3

Here we're creating a subclass of `Pet` called `Dog`, of which we're overriding four methods: `play`, `eat`, `drink` and `sleep`.
As you can notice, all the methods have `&this` passed _explicitly_, and not implicitly,
without the `&this` first parameter, the method would be treated as a static method, and as such would be called on the class, not on an instance of it{{footnote: Like in Zig!}}.

<br/>

Previous:  [Packages and modules](./packages-modules.md) Next: [Generics](./generics.md)
