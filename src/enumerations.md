# Enumerations
Enumerations are a way to express a finite number of options or states;
like in Kotlin, each enum member provides access to its ordinal and name.

```fox
pub enum Animal {
	Cat
	Fox
	Dragon
	Husky
	Horse
}

pub fun main() {
	from stx.libc.stdio import printf
	
	printf( "%s is at position %ud", Animal.Dragon.name, Animal.Dragon.ordinal + 1 )
}
```

##### C-like enums
If you want to use enums as type-safe bit-flags, you can declare them to be `like` a primitive:
```fox
pub enum ColorComponent like u32 {
	Red   = 0xFF000000
	Green = 0x00FF0000
	Blue  = 0x0000FF00
	Alpha = 0x000000FF
}

pub fun extractComponent( color: u32, component: ColorComponent ) u32 =
	color & component

pub fun main() {
	from stx.libc.stdio import printf
	
	color = extractComponent( 0xC2FA9000u32, ColorComponent.Red )
	printf( "Color's red component is %ub", color ) // C2
}
```


##### Tuple-like enum members
```fox
from std.io.console import println

pub enum OptionalInt {
	Int(i32)
	None
}

pub fun main() {
	int = OptionalInt.Int(32)
	
	if ( Int(value) = int )
		println( "we have an int!" )
	else
		println( "we don't have an int.. :<" )
}
```
In this example, we made an enum that may represent a `i32` or nothing,  instantiate its Int variant with a `32`, and pattern-match to check if it has a value.

##### Struct-like enum members
```fox
pub enum ClickEvent {
	Release {
		x: f64
		y: f64
		btn: MouseButton
	}
	Press {
		x: f64
		y: f64
		btn: MouseButton
	}
	Click {
		x: f64
		y: f64
		btn: MouseButton
		clicks: u8
	}
}
```
Here we have a hypothetical event type for mouse clicks, as you can see, with each variant is associated an anonymous struct that stores additional named data.

<br/>

Previous: [Structures](./structures.md) Next: [Packages and Modules](./packages-modules.md)
