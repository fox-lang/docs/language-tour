# Compile time execution
A compliant Fox compiler is able to run user's code at compile time in various ways:
- `comptime $expr` expression, which evaluates to the result of an expression. 
- `comptime $stmt`, which is able to generate code via static reflection, or not emit it basing off contfol flow.
- macros, which replace themselves and their parameters with their body's resulted tokens or AST nodes.

<br/>

Previous: [Generics](./../generics.md) Next: [`comptime`](./comptime.md)
