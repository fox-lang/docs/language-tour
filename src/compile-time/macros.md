# Macros
Macros are one of the most powerful features in fox, and as such need to be used carefully, Fox's macro system is kinda like LISP's, as they can be placed anywhere, act on anything, and be typed.

##### Example
An example of a simple untyped macro that expands to a `println` call
```fox
import println from std.io.console

macro say ( thing ) {
	yield { println( } thing { ) }
}

pub fun main() {
	say "Hello world!" // expands to `::std::io::console::println( "Hello world!" )`
}
```

As you can see, it's quite syntax heavy

<br/>

Previous:  [`comptime`](./comptime.md) Next: [Interoperability](./../interoperability.md)
