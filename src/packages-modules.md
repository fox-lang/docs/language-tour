# Package and Modules
Fox code is organized in packages and modules, they can be thought of like folders and files ( because basically that's what they are ).

Modules are compiled individually, like in C++, but unlike it, there is no need to forward declare members for them to be used, like in Kotlin.

Packages are not required, but are considered best practice, as they help further break down and organize modules, packages may also contain a module called `package` which will be considered the package initializer/content index.

##### Module (de)initializers
Modules may have (de)initializers attached to them, they are special functions, which are ran when the executable the module is in is (un)loaded.    
They are declared like (de)constructors, with module's name where the type name would be.

```fox
fun module() {
	// init code...
}
fun ~module() {
	// cleanup code...
}
```

<br/>

Previous: [Enumerations](./enumerations.md) Next: [Object-Oriented Programming](./oop.md)
