# Structures
Structures are a way of putting together related data, creating a new type.

The classic
-
The structures we're all used to, like those present in C or Rust.

```fox
pub struct Color {
	red: u8
	green: u8
	blue: u8
	alpha: u8
}

pub fun main() {
	from stx.libc.stdio import printf
	
	var color = Color { .red=23, .blue=127, .green=34, .alpha=0 }
	printf( "Red is %ud", color.red )
}
```
Here we can see a basic RGBA8888 struct, which is then instantiated in a `main` function by using the field's names out of order; then, the red component is printed to `stdout` using the libc's `printf` function, we'll just mention it here but will cover interop with other languages ( such as C ) in chapter [11](11.%20Interoperability.md).


Primitive structures
-
Primitive structures are a type-safe way to pass structures as primitives and primitives as structures.
```fox
pub struct Color like u32 {
	red: u8
	green: u8
	blue: u8
	alpha: u8
}

pub fun main() {
	from stx.libc.stdio import printf
	
	var color = Color { .red=23, .blue=127, .green=34, .alpha=0 }
	printf( "Color's integer value is %ud", color as u32 )
}
```
Here, we have modified the first example to cast the struct object to an `u32`, and then print it with `printf` like before.
As you can see it was quite simple, we just added `like u32` to the struct declaration.    
Primitive structures must not be bigger or smaller than the primitive they base on.


<br/>

Previous: [Literals](./stmt-expr/literals.md) Next: [Enumerations](./enumerations.md)
