# Literals
Literals in fox a bit special, like in any other language, there are numeric literals ( `123`, `1.23`, `1.2E+3`, `0x0`, `0334`, `0b010`, `'e'` ),
string literals ( `"hello"`, `"""there"""` ) and boolean literals ( `true`, `false` ).
Something that Fox has, which not all languages have, are _type specifiers for literals_, _user defined literals_ and _templated string literals_.

#### Literals type specifiers
Literals type specifiers are used to specify the type directly in the literal, for example `34u8` is a literal of the number `34` of type `u8`,
one may use any primitive as type specifier, user-defined primitives works too.    
Specifiers may be used, for example, to correct the compiler's type inference on a variable declaration.

#### User-defined literals
User-defined literals are a way to create literals basing off the type specifier syntax; for example:
```fox
pub fun main() {
    var person = """{ "name": "Jonathan", "surname": "Quark" }"""json
}
```
this is one use of user-defined literals, it is equivalent to
```fox
pub fun main() {
    var person = comptime json( """{ "name": "Jonathan", "surname": "Quark" }""" )
}
```
as you can see, the de-sugaring reveals what exactly is happening here, the string is passed as parameter to the `json` function,
which is evaluated at compile time; making `person` be a statically allocated object ( in this case, of type `JsonValue` ).

#### Templated string literals
Like in Kotlin, templated string literals are string literals, but with variables inside them, unlike Kotlin, they require opt-in:
```fox
from std.io.console import println

pub fun main() {
    var age = 32
    var name = "Walter"
    
    println( t"Hi, i'm ${name} and i am ${age} years old"!! )
}
```
the output of this program, as you might correctly guess, is `Hi, i'm Walter and i am 32 years old`.
As memory allocation is fallible, templated string literals actually return a `Result` variant, which in we unwrap in this example with `!!`;
although we can make a note here, as `age` and `name` are compile-time known, they are embedded directly in the string at compile-time; so there is no runtime overhead for this case.

<br/>

Previous: [Expressions](./expressions.md)  Next: [Structures](./../structures.md)
