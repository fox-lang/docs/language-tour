# Expressions

- `if ($expr) $stmt else $stmt`
- `when ($expr) $whenBody`
- `$expr $binOp $expr`
- `$unaryOp $expr`
- `$expr[$expr]`
- `( $expr )`
- `comptime $stmt`
- `{ -> }`
- `{ }`
- `&$expr`

<br/>

Previous: [Statements](./statements.md)  Next: [Literals](./literals.md)
