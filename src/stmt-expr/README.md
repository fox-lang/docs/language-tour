# Statements, Expressions, and literals
In this chapter, we'll go over the statements and expressions available in Fox.

<br/>

Previous: [Functions](./../functions.md)  Next: [Statements](./stmt-expr/statements.md)
