# Statements
Statements are a unit which states an executing action.

`return [$expr]`
-
Ends the currently running function, returning control to the caller.

`continue`
-
Ends the current iteration in the innermost loop.

`break`
-
Ends the innermost loop.

`fallthrough`
-
`when` expressions do not have implicit fallthrough, you can use this statement to explicitly continue to the next case.  

`$expr`
-
An expression.

`$name $assignOp $expr`
-
A variable declaration/assignment.

`loop $stmt`
-
An infinite loop.

`while ( $expr ) $stmt`
-
The classic while loop.

`for ( $varDecl in $expr ) $stmt`
-
A for-each loop.

`yield [$expr|$tokens]` ( WiP: To be decided )
-
The `yield` statement has three diverse effects based on where it is used:
 - case 0, in a loop: End the loop, yielding a value.
 - case 1, in a block: Return a value from a block. 
 - case 2, in a macro: Yield some tokens, we'll talk more about this later in chapter [10.2](../compile-time/macros.md).

<br/>

Previous: [Statements, Expressions and Literals](.)  Next: [Expressions](./expressions.md)
