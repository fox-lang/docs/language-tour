# Hello World
There's a non-written rule in computer science,
which says that every programmer shall start learning a programming language with a "hello world" program,
so let's see it in Fox!


```fox
from std.io.console import println

pub fun main() {
	println( "Hello world!" )
}
```

The program starts with importing the console's `println` function, as Fox, like C++, only pre-imports some basic types by default,
everything else must be explicitly imported.
Then, we declare the parameter-less version of the `main` function, which is the entrypoint of a Fox program, like in most languages,
and is called when your executable is run; it contains a single statement: a call to the previously imported `println` function,
printing to `stdout` the passed string and ending the line.

##### To run this program
- Copy the contents of the code block to a file called `hello.fox`
- Run the shell command `foxc hello.fox -o hello`
- Run the resulted executable with `./hello`

<br/>

Previous: [Introduction](./README.md) Next: [Functions](./functions.md)
