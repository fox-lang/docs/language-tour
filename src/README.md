# Introduction
This tour will give a general overview of Fox's features and constructs,
but also assumes prior experience with other languages such as C++, Java or Kotlin.

This is not a detailed language reference, it doesn't explain everything the language has to offer.

It is worth noting to keep in mind that Fox is a toy language and in an active development state,
so changes _can_ and _will_ occur, only time will tell what will be implemented, what will not and what will be removed.

[//]: # (What s not avaliable has been commented out)
##### Links
Fox language:          <https://gitlab.com/fox-lang/fox-lang>   
Language tour:         <https://fox-lang.gitlab.io/docs/language-tour> (this book)   
Intellij-based plugin: <https://gitlab.com/fox-lang/intellij-fox>   
<!-- Language reference:    <https://fox-lang.gitlab.io/docs/language-reference> -->
<!-- Standard library docs: <https://fox-lang.gitlab.io/docs/std> -->
<!-- TreeSitter parser:     <https://gitlab.com/fox-lang/tree-sitter-fox> -->
<!-- Fox language server:   <https://gitlab.com/fox-lang/fox-language-server> -->

<br/>

Next: [Hello world](./hello_world.md)

