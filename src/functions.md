# Functions
Functions are a way to reuse code, and to encapsulate it in smaller, easier to follow, units.
You've already seen how they are declared in the previous section, but as a refresher:
```fox
pub fun zero( param: *mut u8 ) {
	param = 0
}
```
This declares a function named `zero` that takes in a mutable reference to a value of type `u8`, which then sets to `0` when ran.
The equivalent C code would look something like this
```cpp
void zero( uint8_t* param ) {
	param = 0;
}
```
Let's go a bit though the differences, shall we?
- In C, we are using pointers, which means we can assign integers to them,
  and as such, the above example is not doing what one would expect,
  we are replacing the pointer, not assigning to the pointed memory! Hopefully, you had catched that before reading this. Although its not an error which is difficult to notice in small functions, in bigger ones this may slip though the cracks, causing unexpencted behavior.
  Meanwhile, in Fox land, while having the same bug, it would actually error out, as pointers aren't a superset of integers, and as such trying to assign a number to the pointer is a type error; bellow is the fixed code.
```fox
pub fun zero( param: *mut u8 ) {
	param.* = 0
}
```

- In C, `param` may be `null`, which would cause, using the Java jargon, a `NullPointerException`, Fox fixes this by making `null` its own type: `?T`, this allows to be a little more memory safe than C, while still allowing `null` to co-exist.

- While everything in C is mutable by default, in Fox it's the opposite, everything by default is _immutable_, this makes you, the programmer, think about if a variable should be mutable, more often than not, you'll find that mutability isn't needed.  


Single expression form
-
We've seen that we can declare function bodies using the classic `{block}` construct, but sometimes, that may feel like boilerplate or unneeded, as sometimes we just want to return a single value or, in other words, a single _expression_.
For this reason, in Fox you can also declare functions with just a single expression:
```fox
fun foo() u8 =
    0
```
As you can see, we basically _assign_ the expression we want to return to the function.

Polymorphism
-
Functions in Fox support many ways of doing polymorphism, one of which is function overloading.
```fox
fun foo( obj: String ) {
    printf( "Called with string: %s", obj )
}
fun foo( obj: usize ) {
    printf( "Called with usize: %lld", obj )
}
```
As you can see, we declared a function with the same name twice, but they have different parameter types


Passing functions around
-
In Fox, functions are first-class citizens; that means that can be passed around as values; to do that; we need a way to declare a function's type, which Fox has a handy way of declaring:
```fox
fun newThread( target: fun() void ) ThreadHandle { ... }
```
Here, we have a function that will spawn a new thread, with the given function as its main, and returns a handle to it.
The syntax, as you can see, it's quite simple and intiutive, its just a function declaration without the name.   
Although not required, parameters may also be named for convenience:
```fox
fun listenKeyPress( listener: fun(key: char, modifiers: u8) void ) { ... }
// same as
fun listenKeyPress( listener: fun(char, u8) void ) { ... }
```
As said, although useful, parameter names in function types is neither required nor used during compilation, names are merely a way to convey more meaning of what a function type expects.

Lambdas
-
Lambdas are purely nameless functions, and as such, all data they access must either be globally stored, or passed as arguments.
```fox
var func: fun(char, u8) void = { key, modifiers -> // parameters are implicity typed
    printf( "key: %s  modifiers: %d", key, modifiers )
}
```
As you can see, they work like almost any other language, Fox's lambdas and closures are heavely inspired by Kotlin's same feature.

### Closures
```fox
fun main( argv: []String ) {
    var func = { ->
        printf( "exec path: %s", argv[0] )
    }
    func()
}
```
<br/>

Previous:  [Hello world](./hello_world.md) Next: [Statements and Expressions](./stmt-expr)
