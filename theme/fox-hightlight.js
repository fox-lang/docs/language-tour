"use-strict";

hljs.registerLanguage( 'fox', hljs => ({
	name: 'Fox',
	aliases: [ 'fox' ],

	keywords: {
		keyword: 'as break class comptime continue else enum fallthrough for from fun if import interface like macro mut override pub reinterpret return struct typealias when while',
		built_in: 'u8 u16 u32 u64 u128 i8 i16 i32 i64 i128 f32 f64 bool void auto',
		literal: 'false true',
	},

	contains: [
		{
			scope: 'string',
			begin: /"""/,
			end: /"""/,
			contains: [ hljs.BACKSLASH_ESCAPE ]
		},
		{
			scope: 'string',
			begin: /"/,
			end: /"/,
			contains: [ hljs.BACKSLASH_ESCAPE ]
		},
		hljs.C_BLOCK_COMMENT_MODE,
		hljs.C_LINE_COMMENT_MODE,
		hljs.C_NUMBER_MODE
	]
}));


hljs.initHighlightingOnLoad();
